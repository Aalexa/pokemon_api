function getPokemon(pokemon) {
    fetch('https://pokeapi.co/api/v2/pokemon/' + pokemon)
        .then(response => response.json())
        .then(data => {
            updateCard(data);
        });
}

function getAbilities(ability) {
    return fetch(ability.ability.url)
        .then(response => response.json())
        .then(data => {
            const description = data.effect_entries.find(item => item.language.name === 'en').effect;
            let html = `<p> <span> ${ability.ability.name} </span> ${description} </p>`;
            document.querySelector('.card .attacks').innerHTML += html;
        });
}

function searchClick() {
    const inputValue = document.getElementById('search').value;

    getPokemon(inputValue);
}

function updateCard(apiResponse) {
    if(apiResponse.id){
        changeCard(true);
    }else{
        changeCard();
    }

    document.querySelector('.card .hp').innerText = apiResponse.stats.find(item => item.stat.name === 'hp').base_stat;
    document.querySelector('.card .title').innerText = apiResponse.name;
    document.querySelector('.card .type').innerText = apiResponse.types[0].type.name;
    document.querySelector('.card .image').src = apiResponse.sprites.front_default;

    const attacksContainer = document.querySelector('.card .attacks');
    attacksContainer.innerHTML = '';

    apiResponse.abilities.forEach(ability => {
        getAbilities(ability);
    });
}

function nextPrevPokemon(right = false){
    let search = document.getElementById('search');
    let value = Number(search.value);

    if(isNaN(value)){
        value = 1;
    }

    if (right){
        value += 1;
    }else{
        value -= 1;
    }

    search.value = value;

    getPokemon(value);
}

function changeCard(enable = false){
    if(enable){
        document.querySelector('.card').style.display = 'block';
        document.querySelector('.cardBackground').style.display = 'none';
    }else{
        document.querySelector('.card').style.display = 'none';
        document.querySelector('.cardBackground').style.display = 'block';
    }
}

document.getElementById('search').addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      searchClick()
    }
  });